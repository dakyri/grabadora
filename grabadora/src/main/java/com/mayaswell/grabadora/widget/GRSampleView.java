package com.mayaswell.grabadora.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.widget.BaseSampleView;

/**
 * Created by dak on 4/12/2017.
 */
public class GRSampleView extends BaseSampleView {
	private SamplePlayer currentPlayer = null;
	protected int crsX = 0;

	public GRSampleView(Context context) {
		super(context);
	}

	public GRSampleView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public GRSampleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	/**
	 * updates cursor and invalidates the appropriate regions
	 */
	public void updateCursor() {
		invalidate(crsX - 1, 0, crsX + 1, svH);
		updateCursorX();
		invalidate(crsX - 1, 0, crsX + 1, svH);
	}

	/**
	 * updates the current cursor position of the current player
	 */
	protected void updateCursorX() {
		SamplePlayer p = currentPlayer;
		if (p != null) {
			crsX = (int) (pxRs + p.getCurrentPosition() * (pxRe - pxRs));
//			Log.d("cursor", String.format("%d = %g %g %g", crsX, currentPlayer.getCurrentPosition(), pxRe, pxRs));
		}
	}

	public void setPlayer(SamplePlayer q) {
		currentPlayer = q;
		if (q != null) {
			setRegionStart(q.getRegionStart());
			setRegionLength(q.getRegionLength());
		}
	}


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (crsX >= 0 && crsX <= svW) {
			canvas.drawLine(crsX, 0, crsX, svH, cursorBrush);
		}

	}


	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		setPlayer(currentPlayer);
	}
}