package com.mayaswell.grabadora.widget;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.Bufferator.SampleGfxInfo;
import com.mayaswell.audio.Bufferator.SampleInfo;
import com.mayaswell.grabadora.MainActivity;
import com.mayaswell.grabadora.R;
import com.mayaswell.spacegun.PadSample;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.widget.TeaPot;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by dak on 6/1/2016.
 */
public class SampleItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

	public static class SampleViewHolder extends RecyclerView.ViewHolder {
		public SampleViewHolder(RelativeLayout v) {
			super(v);
			setIsRecyclable(false);
			v.setOnCreateContextMenuListener(((Activity)v.getContext()));
		}
	}

	protected ArrayList<PadSample> padList = null;
	private ArrayList<PadSampleState> padStateList = null;
	private ArrayList<SampleInfo> sampleInfoList = null;

	/**
	 * sets the padState list of this list ... we don't expect this to change, though the PadSampleState may change.. PadSample
	 * wraps the main jni interface for audio generation, so accesses need to be through it
	 * @param pl
	 */
	public void setPadList(ArrayList<PadSample> pl) {
		padList = pl;
		if (pl != null) {
			for (int i = 0; i < pl.size(); i++) {
				PadSample p = pl.get(i);
				if (!equalState(p.getState(), padState(i))) {
//					setPadState(i, p.getState(), p.getSampleInfo());
					notifyItemChanged(i);
				}
			}
		}
	}

	public void setPadStateList(ArrayList<PadSampleState> pl, ArrayList<SampleInfo> sl) {
		padStateList = pl;
		sampleInfoList = sl;
		if (pl != null) {
			for (int i = 0; i < pl.size(); i++) {
				notifyItemChanged(i);
				/*
				PadSample p = pl.get(i);
				if (!equalState(p.getState(), padState(i))) {
//					setPadState(i, p.getState(), p.getSampleInfo());
					notifyItemChanged(i);
				}*/
			}
		}
	}

	/**
	 * @param sampleInfo
	 */
	public void setGfx4SampleInfo(SampleInfo sampleInfo) {
//		Log.d("SampleAdapter", "setting gragix");
		if (padStateList != null) {
			for (int i = 0; i < padStateList.size(); i++) {
				PadSampleState p = padStateList.get(i);
				if (p.path != null && p.path.equals(sampleInfo.path)) { // actual equality, typicall padsample is an unchanging object
//					Log.d("adapter", "gx item changed");
					notifyItemChanged(i);
					return;
				}
			}
		}
	}

	public void checkPadSample(PadSample pad) {
		int i = padList.indexOf(pad);
		if (i >= 0) {
			if (!equalState(padList.get(i).getState(), pad.getState())) {
				notifyItemChanged(i);
			}
		}
	}

	/**
	 * callback when a PadSample is loaded
	 * @param pad
	 */
	public void onPadSampleLoaded(PadSample pad) {
		notifyDataSetChanged();
//		Log.d("SampleAdapter", "onPadSampleLoaded "+pad.getState().path+", "+padList.size());
		int i;
		if ((i = padList.indexOf(pad)) >= 0) {
//			setPadState(i, pad.state, pad.getSampleInfo());
			notifyItemChanged(i);
		}
	}

	public void onPadStateAdded(PadSampleState ps, SampleInfo si) {
		notifyDataSetChanged();
//		Log.d("SampleAdapter", "onPadStateAdded "+ps.path+", "+padStateList.size());
		if (padStateList == null) return;
		int i = padStateList.indexOf(ps);
		if (i >= 0) {
//			setPadState(i, pad.state, pad.getSampleInfo());
			notifyItemChanged(i);
		}
	}

	public void updatePadState(PadSampleState ps) {
		if (padStateList == null) return;
		int i = padStateList.indexOf(ps);
		if (i >= 0) {
//			setPadState(i, pad.state, pad.getSampleInfo());
			notifyItemChanged(i);
		}
	}

	public void removePadState(PadSampleState ps) {
		if (padStateList == null) return;
		int i = padStateList.indexOf(ps);
		Log.d("adapter", "remove pad state "+i+" "+ps.path);
		if (i >= 0) {
//			setPadState(i, pad.state, pad.getSampleInfo());
			notifyItemRemoved(i); // afics this should work but doesn't
			padStateList.remove(i);
			notifyDataSetChanged(); // but this does
		}
	}

	private boolean equalState(PadSampleState p1, PadSampleState p2) {
		if (p1 == null || p2 == null) {
			return false;
		}
		if (p1.pan == p2.pan && p2.gain == p2.gain && p2.path.equals(p1.path)) {
			return true;
		}
		return false;
	}

	private PadSampleState padState(int i) {
		return (i>=0 && padStateList != null && i<padStateList.size()? padStateList.get(i): null);
	}

	private SampleInfo sampleInfo(int i) {
		return (i>=0 && sampleInfoList != null && i<sampleInfoList.size()? sampleInfoList.get(i): null);
	}

	private PadSample samplePlayer(int i) {
		return (i>=0 && padList != null && i<padList.size()? padList.get(i): null);
	}


	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//		Log.d("SampleAdapter", "onCreateViewHolder "+viewType+", "+padStateList.size());
		RecyclerView.ViewHolder vh = null;
		RelativeLayout v = new SampleItemView(parent.getContext());
		vh = new SampleViewHolder(v);
		return vh;
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//		Log.d("SampleAdapter", "onBindViewHolder "+position+", "+padStateList.size());
		if (position >= padStateList.size()) {
			return;
		}
//		int type = getItemViewType(position);
		PadSampleState s = padState(position);
		SampleItemView itemView = (SampleItemView)((SampleViewHolder) holder).itemView;
		boolean sel = (s != null) && ((MainActivity)itemView.getContext()).getSelectedState() == s;
//		Log.d("SampleAdapter", "onBindViewHolder selected is "+sel);
		itemView.setTo(s, sampleInfo(position), samplePlayer(position), sel);

		holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
//				setPosition(holder.getPosition());
				Log.d("holder", "on long click");
				return false;
			}
		});
	}

	@Override
	public void onViewRecycled(RecyclerView.ViewHolder holder) {
		holder.itemView.setOnLongClickListener(null);
		super.onViewRecycled(holder);
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	@Override
	public int getItemCount() {
		if (padStateList == null || padStateList.size() == 0) {
			return 0;
		}
		int i=padStateList.size()-1;
		for (;i>=0; i--) {
			PadSampleState p = padStateList.get(i);
			if (p != null && p.path != null && !p.path.equals("")) {
				break;
			}
		}
		return i+1;
	}

}
