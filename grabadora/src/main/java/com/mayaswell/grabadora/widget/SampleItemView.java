package com.mayaswell.grabadora.widget;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.grabadora.MainActivity;
import com.mayaswell.grabadora.R;
import com.mayaswell.spacegun.PadSampleState;

import java.io.File;

/**
 * Created by dak on 10/8/2016.
 */
public class SampleItemView extends RelativeLayout implements ContextMenuInfo {
	private static final int[] STATE_ACTIVE = {R.attr.state_active};
	private static final int[] STATE_PLAYING = {R.attr.state_playing};

	protected PadSampleState padState;
	protected GRSampleView sampleView;
	private TextView sampleTitleView = null;
	private SampleItemView self;
	private boolean inScroll = false;

	public SampleItemView(Context context) {
		super(context);
		init(context, null, -1);
	}

	public SampleItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, -1);
	}

	public SampleItemView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs, defStyleAttr);
	}

	private void init(Context context, AttributeSet attrs, int defStyleAttr) {
		LayoutInflater.from(context).inflate(R.layout.sample_list_item, this, true);
		setBackgroundResource(R.drawable.sample_list_item_bg);
		sampleView = (GRSampleView) findViewById(R.id.sampleView);
		sampleTitleView = (TextView) findViewById(R.id.titleView);
		LayoutParams rl = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		rl.setMargins(0, 5, 0, 5);
		setLayoutParams(rl);
		self = this;
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					((MainActivity) getContext()).select(self);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				Log.d("siv", "long click");
				return false;
			}
		});
		refreshDrawableState();
	}

	@Override
	protected ContextMenuInfo getContextMenuInfo() {
		return  this;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		inScroll = false;
		Log.d("SampleItemView", self.toString()+" onInterceptTouchEvent()");
		return super.onInterceptTouchEvent(ev);
	}

	@Override
	protected void onAttachedToWindow() {
		ViewParent parent = getParent();
		if (parent != null) {
			((RecyclerView)parent).addOnScrollListener(new RecyclerView.OnScrollListener() {
				@Override
				public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
					inScroll = true;
					super.onScrollStateChanged(recyclerView, newState);
				}
			});
		}
		super.onAttachedToWindow();
	}

	public void setTo(PadSampleState p, Bufferator.SampleInfo q, SamplePlayer r, boolean selected) {
		setSelected(selected);
		padState = p;
		File f = new File(p.path);
		sampleTitleView.setText(f.getName());
		if (sampleView != null) {
			sampleView.setTo(q);
			sampleView.setPlayer(r);
		}
		refreshDrawableState();
	}

	/**
	 * @see android.widget.TextView#onCreateDrawableState(int)
	 */
	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
//		Log.d("SampleItemView", "onCreateDrawableState "+toString()+" active:"+isActive()+", play:"+isPlaying()+", select:"+isSelected());
		if (isActive()) {
			mergeDrawableStates(drawableState, STATE_ACTIVE);
		}
		if (isPlaying()) {
			mergeDrawableStates(drawableState, STATE_PLAYING);
		}
		return drawableState;
	}

	public String toString() {
		String nm = "<>";
		if (padState != null && padState.path != null) {
			nm = padState.path.toString();
			int n = nm.lastIndexOf('/');
			if (n > 0) {
				nm = nm.substring(n);
			}
		}
		return "sv "+ nm;
	}
	/**
	 */
	@Override
	protected void drawableStateChanged ()
	{
		super.drawableStateChanged();
	}


	public boolean isActive()
	{
		SamplePlayer player = ((MainActivity) getContext()).getPadSample4(padState);
		return player != null;
	}


	public boolean isPlaying()
	{
		SamplePlayer player = ((MainActivity) getContext()).getPadSample4(padState);
		return player != null && player.isPlaying();
	}

	public boolean select() {
		setSelected(true);
		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event){
		return super.onTouchEvent(event);
//		return detector.onTouchEvent(event);
	}

	public PadSampleState getPadState() {
		return padState;
	}

	public void updateCursor() {
		sampleView.updateCursor();
	}
}