package com.mayaswell.grabadora;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.Bufferator.SampleInfo;
import com.mayaswell.audio.Metric;
import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.audio.file.AudioFile;
import com.mayaswell.grabadora.widget.SampleItemAdapter;
import com.mayaswell.grabadora.widget.SampleItemView;
import com.mayaswell.widget.PlaPriRecButton;
import com.mayaswell.spacegun.BusMixer;
import com.mayaswell.spacegun.DBRXAudioMixer;
import com.mayaswell.spacegun.PadSample;
import com.mayaswell.spacegun.PadSampleState;
import com.mayaswell.util.AbstractBank;
import com.mayaswell.util.ErrorLevel;
import com.mayaswell.widget.MeterView;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static com.jakewharton.rxbinding.view.RxView.clicks;

public class MainActivity extends AppCompatActivity implements SamplePlayer.Manager {
	protected static String []aboutButtonText = {"Whatever", "Yeah, right", "Did I really need to know that?", "Did you find that in a cereal box?"};

	private PadSampleState selectedPadState = null;
	private BusMixer busMixer = null;

	private String sampleNameBase;

	protected  EditText saveFileEditText = null;

	public PadSampleState getSelectedState() {
		return selectedPadState;
	}

	public class Global {
		public static final int N_PAD = 1;
		public static final int LFO_PER_PAD = 4;
		public static final int MAX_LFO_TGT = 4;
		public static final int ENV_PER_PAD = 2;
		public static final int MAX_ENV_TGT = 4;
		public static final int STPR_PER_PAD = 2;
		public static final int MAX_STPR_TGT = 4;
		public static final int MAX_BUS = 4;
		public static final int MAX_BUS_FX = 4;
		public static final int MAX_FX_PARAM = 6;
	}


	private DBRXAudioMixer audioMixer = null;
	private ArrayList<PadSample> padSample = null;
	protected ArrayList<PadSampleState> padStateList = new ArrayList<PadSampleState>();
	protected ArrayList<Bufferator.SampleInfo> sampleInfoList = new ArrayList<Bufferator.SampleInfo>();

	private Bufferator bufferator = null;

	private int lastRecFileNumber = 0;
	private File currentBankFile = null;
	private int currentErrorSeverity = 0;

	private PlaPriRecButton recButton = null;
	private PlaPriRecButton goButton = null;
	private MeterView meterView = null;

	private RelativeLayout mainView = null;
	private RelativeLayout controlPanel = null;
	private RelativeLayout sampleItemListScroller = null;

//	private RelativeLayout fragmentWrapper = null;

//	private SampleViewFragment sampleViewFragment;

	private SampleItemAdapter sampleItemAdapter	= null;
	private LinearLayoutManager sampleItemLayoutManager	= null;
	private RecyclerView sampleItemView	= null;
	private TextView beatCounter = null;

	private Typeface digitalDream = null;

	private Metric metric = new Metric(60);

	public static final int NO_ACTION = 0; // non-action
	public static final int R_ACTION = 1; // single press on the rec button
	public static final int G_ACTION = 2; // sinlge press on the go button
	public static final int RG_ACTION = 3; // simultaneous press of rec and go
	public static final int RR_ACTION = 4; // double click on rec button
	public static final int GG_ACTION = 5; // double click on go button

	/**
	 * class for passing around the base actions from interface, automatically timestamped
	 */
	public static class IntAct {
		private final long ts;
		private final int type;

		IntAct (int t) {
			this(t, System.currentTimeMillis());
		}
		IntAct(int t, long ts) {
			type = t;
			this.ts = ts;
		}
	}

	public MainActivity() {
		super();
		Log.d("Main Activity", "Constructing ...");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d("MainActivity", "onCreate "+(savedInstanceState!=null?savedInstanceState.toString():"<>"));
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// todo convert these bits to butterknife annotations
		mainView = (RelativeLayout) findViewById(R.id.mainView);
		recButton = (PlaPriRecButton) findViewById(R.id.recButton);
		goButton = (PlaPriRecButton) findViewById(R.id.goButton);
		controlPanel = (RelativeLayout) findViewById(R.id.controlPanel);
		sampleItemView = (RecyclerView) findViewById(R.id.sampleItemListView);
		meterView = (MeterView) findViewById(R.id.meterView);
		beatCounter = (TextView) findViewById(R.id.beatCounter);
		sampleItemListScroller = (RelativeLayout) findViewById(R.id.sampleItemListScroller);

//		fragmentWrapper = (RelativeLayout) findViewById(R.id.fragmentWrapper);

		recButton.setSoundEffectsEnabled(false);
		goButton.setSoundEffectsEnabled(false);

		Observable<IntAct> goObservable = clicks(goButton).map(new Func1<Void, IntAct>() {
			@Override
			public IntAct call(Void aVoid) {
				return new IntAct(G_ACTION);
			}
		});
		Observable<IntAct> recObservable = clicks(recButton).map(new Func1<Void, IntAct>() {
			@Override
			public IntAct call(Void aVoid) {
				return  new IntAct(R_ACTION);
			}
		});
		Observable.merge(goObservable, recObservable)
						// catch burst adapt from https://gist.github.com/benjchristensen/e4524a308456f3c21c0b
				.publish(new Func1<Observable<IntAct>,Observable<List<IntAct>>>() {
								// publish allows for a multicast within the function
					@Override
					public Observable<List<IntAct>> call(Observable<IntAct> stream) {
						return stream.buffer(			// buffer groups into burst according to the close function, which is
								stream.debounce(200, TimeUnit.MILLISECONDS) // this debounce, marking the end of bursts
						);
					}
				})
				// this filter originally needed now for the rough timed buffer op solution, which
				// generated null buffers. but also would fail to group correctly as it ran on a fixed time
				// window.
				/*
				.filter(new Func1<List<IntAct>, Boolean>() {
					@Override
					public Boolean call(List<IntAct> o) {
						return o.size() > 0;
					}
				})*/
				// map the burst lists onto single actions, keeping the timestamp of the oldest member of the list
				.map(new Func1<List<IntAct>, IntAct>() {
					@Override
					public IntAct call(List<IntAct> acts) {
						if (acts.size() == 0) {
							return new IntAct(NO_ACTION);
						} else if (acts.size() == 1) {
							return acts.get(0);
						} else {
							int t = acts.get(0).type;
							int tt = NO_ACTION;
							for (IntAct i: acts) {
//								Log.d("MainActivity", "click list element "+i.type+", "+i.ts);
								if (i.type != t) {
									tt = RG_ACTION;
								}
							}
							if (tt == NO_ACTION) {
								tt = (t== G_ACTION ? GG_ACTION : RR_ACTION);
							}
							return new IntAct(tt, acts.get(0).ts);
						}
					}
				} )
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new Action1<IntAct>() {
					@Override
					public void call(IntAct act) {
						switch (act.type) {
							case G_ACTION: {
								if (selectedPadState == null) {
									if (padStateList.size() > 0) {
										selectedPadState = padStateList.get(0);
									}
								}
								if (selectedPadState != null) {
									PadSample p = getPadSample4(selectedPadState);
									if (p == null) {
										Log.d("MainActivity", "loading selected state into new PadSample");
										p = getFreePadSample(true);
										p.setState(selectedPadState);
										SampleItemView sv = getSampleView4(selectedPadState);
										if (sv != null) {
											sv.refreshDrawableState();
										}

									}
									if (p != null) {
										if (!p.isPlaying()) {
											Log.d("MainActivity", "PadSample for selected state to be started");
											audioMixer.startPad(p, false);
										} else {
											Log.d("MainActivity", "PadSample for selected state to be stopped");
											audioMixer.stopPad(p);
											p.stop(false);
										}
										setGoButtonState(p);
										getSampleView4(selectedPadState).refreshDrawableState();
									}
								}
								break;
							}
							case R_ACTION: {
								if (isRecording()) {
									stopRecording(act.ts);
								} else {
									startRecording(act.ts);
								}
								break;
							}
							case RG_ACTION: {
								break;
							}
							case RR_ACTION: {
								break;
							}
							case GG_ACTION: {
								break;
							}
						}
						Log.d("MainActivity", "done action "+act.type+" at "+act.ts);
					}
				});

		sampleItemAdapter = new SampleItemAdapter();
		sampleItemView.setHasFixedSize(true);
		sampleItemLayoutManager = new LinearLayoutManager(this);
		sampleItemView.setLayoutManager(sampleItemLayoutManager);
		sampleItemView.setAdapter(sampleItemAdapter);

//		sampleViewFragment = new SampleViewFragment();

		digitalDream  = Typeface.createFromAsset(getAssets(), "fonts/DigitalDreamFatNarrow.ttf");
		if (beatCounter != null) {
			beatCounter.setText("0:0.00");
			if (digitalDream != null) {
				beatCounter.setTypeface(digitalDream);
			}
		}

		if (audioMixer == null) {
			audioMixer = new DBRXAudioMixer(Global.N_PAD);
			audioMixer.getVU().subscribe(new Action1<Float>() {
				@Override
				public void call(Float aFloat) {
					meterView.setValue(aFloat);
				}
			});
		}

		if (padSample == null) {
			Log.d("Main", "Rebuild pad sample!!!");
			PadSample.globalCPadInit();
			padSample = new ArrayList<PadSample>();
			padStateList = new ArrayList<PadSampleState>();
			sampleInfoList = new ArrayList<SampleInfo>();
			sampleItemAdapter.setPadList(padSample);
			sampleItemAdapter.setPadStateList(padStateList, sampleInfoList);
			int i = 0;
			int resId;
			while (i < Global.N_PAD) {
				PadSample pbs = new PadSample(this, "Pad " + Integer.toString(i), i);
				pbs.setListener(new PadSample.Listener() {
					@Override
					public void playStarted(final PadSample p) {// we've started it elsewhere, just want to update gfx
// when this comes in, we need to
// - start the cursor in the sample display runnning
//						((PadButton)p.padButton).setPlayState(true);
						Log.d("MainActivity", "got a playStarted() "+(padSample != null? padSample.toString():"<>"));
					}

					/*
					case PadSample.PLAY_START_SYNCED: {
// we want to start it elsewhere and we don't want a synchronization clusterfuck there is a messaging lag here though
						player.startPad(pbs);
						pb.setPlayState(pbs.isPlaying());
						return true;
					}*/

					@Override
					public void playComplete(final PadSample padSample) {
// just need to
// - stop cursor updates on this sample
//						stopPad((PadButton) padSample.padButton);
						Log.d("MainActivity", "got a playComplete() "+(padSample != null? padSample.toString():"<>"));
						if (padSample.getState() == selectedPadState) {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									setGoButtonState(padSample);
								}
							});
						}
					}

					@Override
					public void setSampleData(final PadSample padSample) {
						if (sampleItemAdapter != null) {
							sampleItemAdapter.onPadSampleLoaded(padSample);
						}
					}
				});
				padSample.add(pbs);
				i++;
			}
		} else {
			Log.d("MainActivity", "Got pad state list "+(padStateList != null? padStateList.size():0));
		}
		if (busMixer == null) {
			BusMixer.globalBmxInit();
			busMixer = BusMixer.build(Global.MAX_BUS, Global.MAX_BUS_FX, Global.MAX_FX_PARAM,
					Global.LFO_PER_PAD, Global.MAX_LFO_TGT, audioMixer.getOutBufsize());
		}
		sampleNameBase = "sample";

		for (PadSample p: padSample) {
			p.setFreeBuffersOnUnload(false);
			p.setBufsize(audioMixer.getOutBufsize());
		}

		if (bufferator == null) {
			bufferator = Bufferator.init(this);
//			final MWBPActivity<?,?,?> activity = this;
			bufferator.monitorState(new Action1<Bufferator.BufferatorException>() {
				@Override
				public void call(Bufferator.BufferatorException e) {
					e.printStackTrace();
					if (e.severity == ErrorLevel.FNF_ERROR_EVENT) {
						/*
						String si = e.getPath();
						bankSanitizer = BankSanitizer.getInstance();
						if (!BankSanitizer.isChecking()) {
							BankSanitizer.check(currentBank, activity, currentPatch, si);
						}*/
					} else {
						handleErrorMessage(e.getMessage(), e.severity);
					}
				}
			});
			bufferator.monitorGfx(new Action1<Bufferator.SampleInfo>() {
				@Override
				public void call(Bufferator.SampleInfo sampleInfo) {
					Log.d("main", "got data for "+sampleInfo.path+" "+sampleInfo.gfxCache.dataMax[0].length+" points");
					sampleItemAdapter.setGfx4SampleInfo(sampleInfo);
				}
			});
		}
/*
		try {
			Drawable t = ResourcesCompat.getDrawable(getResources(), R.drawable.sample_list_item_bg, null);
			Log.d("main", "got drawable");
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("main", ":(((( drawable");
		}
		*/
	}

	Subscription periodicUpdater = null;
	@Override
	protected void onStart() {
		super.onStart();
		audioMixer.run();
		bufferator.run();
		startPeriodicUpdate();
	}

	@Override
	protected void onResume() {
		super.onResume();
		startPeriodicUpdate();
	}

	@Override
	protected void onPause() {
		super.onPause();
		stopPeriodicUpdate();
	}

	@Override
	protected void onStop() {
		super.onStop();
		stopPeriodicUpdate();
	}

	@Override
	protected void onDestroy() {
		audioMixer.stop();
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		if (isRecording()) {
			bailAlog("All done then?");
			return;
		}
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.sg_menu_basename: {
				basenameDialog();
				return true;
			}
			case R.id.sg_menu_about: {
				showAbout();
				return true;
			}
			case R.id.sg_menu_privacy: {
				showTextDialog(getResources().getString(R.string.privacy_policy_title), R.string.privacy_policy_text);
				return true;
			}
			case R.id.sg_menu_preferences: {
//				Intent i = new Intent(this, SGPreferenceActivity.class);
//				startActivityForResult(i, R.id.sg_menu_preferences);
				return false;
			}
			case R.id.sg_menu_exit: {
				bailAlog("All done then?");
				return true;
			}
			default: {
//			Log.d("option", "Unhandled menu option"+Integer.toString(item.getItemId()));
			}
		}
		return super.onOptionsItemSelected(item);

	}


	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();

//		Log.d("main", "onCreateContextMenu " + v.toString());
		if (v instanceof SampleItemView) {
			inflater.inflate(R.menu.sample_item_context_menu, menu);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		Object v = item.getMenuInfo();
		if (v != null && v instanceof SampleItemView) {
			SampleItemView sv = (SampleItemView) v;
			switch (item.getItemId()) {
				case R.id.gr_item_play: {
					Log.d("onContextItemSelected", "play " + sv.toString());
					PadSample p = getPadSample4(sv.getPadState());
					if (p == null) {
						p = getFreePadSample(true);
						p.setState(sv.getPadState());
						sv.refreshDrawableState();
					}
					if (p != null) {
						if (!p.isPlaying()) {
							Log.d("MainActivity", "PadSample for selected state to be started");
							audioMixer.startPad(p, false);
						} else {
							Log.d("MainActivity", "PadSample for selected state to be stopped");
							audioMixer.stopPad(p);
							p.stop(false);
						}
						if (sv.getPadState() == selectedPadState) {
							setGoButtonState(p);
						}
						sv.refreshDrawableState();
					} else {
						Log.d("MainActivity", "couldn't load pad sample for "+sv.toString()+" into a pad sample");
					}
					return true;
				}
				case R.id.gr_item_delete: {
					Log.d("onContextItemSelected", "delete " + sv.toString());
					deleteDialog(sv);
					return true;
				}
				case R.id.gr_item_rename: {
					Log.d("onContextItemSelected", "rename " + sv.toString());
					renameDialog(sv);
					return true;
				}
			}
		}
		return false;

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		Log.d("MainActivity", String.format("onSaveInstanceState()"));
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList("padStateList", padStateList);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedState) {
		Log.d("MainActivity", String.format("onRestoreInstanceState()"));
		super.onRestoreInstanceState(savedState);
		padStateList = savedState.getParcelableArrayList("padStateList");
		sampleInfoList = new ArrayList<SampleInfo>();
		sampleItemAdapter.setPadStateList(padStateList, sampleInfoList);
		Log.d("MainActivity", "restore state "+padStateList.size());
		for (PadSampleState ps: padStateList) {
			PadSample s = getFreePadSample();
			if (s != null) {
							/*)
							PadSampleState ps = s.getState();
							ps.path = audioFile.getPath();
							s.setState(ps);
							*/
				s.setState(ps);
				setPadState(padSample.indexOf(s), s.state, s.getSampleInfo());
				sampleItemAdapter.onPadSampleLoaded(s);
				if (selectedPadState == null) {
					select(s.getState());
				}
			} else {
				Bufferator.SampleInfo si = Bufferator.load(ps.path);
				int i = setPadState(-1, ps, si);
				Log.d("MainActivity", "restore state pad state  " + i);
				sampleInfoList.add(si);
				sampleItemAdapter.onPadStateAdded(ps, si);
				if (selectedPadState == null) {
					select(ps);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		Log.d("Grabadora", String.format("on low memory"));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onTrimMemory(int level) {
		super.onTrimMemory(level);
		Log.d("Grabadora", String.format("on trim memory %d", level));
	}


	protected Action1<Throwable> onError = new Action1<Throwable>() {
		@Override
		public void call(Throwable e) {
//			Log.d("checkBoundingBox", "got error");
			e.printStackTrace();
		}
	};
	protected void bailAlog(String string)
	{
		AlertDialog.Builder d = new AlertDialog.Builder(this);
		d.setTitle("");
		d.setMessage(string);
		d.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.setNeutralButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				finish();
			}
		});
		d.show();
	}

	private void renameDialog(SampleItemView sv) {
		final PadSampleState padSampleState = sv.getPadState();
		if (padSampleState == null) {
			Log.d("Main", "unexpected null sample state in delete");
		}
		final String path = padSampleState.path;
		if (padSampleState.path == null) {
			Log.d("Main", "unexpected null path in delete");
		}
		File f = new File(path);

		if (saveFileEditText == null) {
			saveFileEditText = new EditText(this);
		}

		String name = f.getName();
		int suffind = name.lastIndexOf('.');
		String sfx = "";
		if (suffind > 0) {
			sfx = name.substring(suffind+1);
			name = name.substring(0, suffind);
		}
		final String suffix = sfx != null? sfx:"";

		saveFileEditText.setHint(name);
		saveFileEditText.setText(name);

		AlertDialog.Builder d = new AlertDialog.Builder(this);
		d.setTitle("Rename");
		d.setMessage("Enter new name for "+f.getName()+":");
		ViewGroup vp = (ViewGroup) saveFileEditText.getParent();
		if (vp != null) {
			vp.removeView(saveFileEditText);
		}
		d.setView(saveFileEditText);

		d.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String tfnm = saveFileEditText.getText().toString();
				if (tfnm != null && !tfnm.equals("")) {
					renameSampleFile(padSampleState, tfnm, suffix);
					sampleItemAdapter.updatePadState(padSampleState);
					setSampleFileNameBase(tfnm);
				}
			}
		});
		d.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show();
	}

	private boolean renameSampleFile(PadSampleState padSampleState, String tfnm, String suffix) {
		if (padSampleState == null || tfnm == null) {
			return false;
		}

		File f = new File(padSampleState.path);
		String parent = f.getParent();
		String newPath = parent+"/"+tfnm+"."+suffix;
		PadSample s = getPadSample4(padSampleState);
		Log.d("main", "reaname new "+newPath);
		File fn = new File(newPath);
		if (fn.exists()) {
			return false;
		}
		f.renameTo(fn);

		int ind = padStateList.indexOf(padSampleState);
		SampleInfo inf = sampleInfoList.get(ind);
		inf.setPath(newPath);

		if (s != null) {
			s.setSamplePath(newPath);
		} else {
			padSampleState.path = newPath;
		}


		return true;
	}

	private void basenameDialog() {

		if (saveFileEditText == null) {
			saveFileEditText = new EditText(this);
		}

		saveFileEditText.setHint(sampleNameBase);
		saveFileEditText.setText(sampleNameBase);

		AlertDialog.Builder d = new AlertDialog.Builder(this);
		d.setTitle("Set sample name base");
		d.setMessage("Enter new sample file name base:");
		ViewGroup vp = (ViewGroup) saveFileEditText.getParent();
		if (vp != null) {
			vp.removeView(saveFileEditText);
		}
		d.setView(saveFileEditText);

		d.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String tfnm = saveFileEditText.getText().toString();
				if (tfnm != null && !tfnm.equals("")) {
					setSampleFileNameBase(tfnm);
				}
			}
		});
		d.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show();
	}



	private void deleteDialog(final SampleItemView sv) {
		final PadSampleState padSampleState = sv.getPadState();
		if (padSampleState == null) {
			Log.d("Main", "unexpected null sample state in delete");
		}
		final String path = padSampleState.path;
		if (padSampleState.path == null) {
			Log.d("Main", "unexpected null path in delete");
		}
		AlertDialog.Builder d = new AlertDialog.Builder(this);
		d.setTitle("Delete");
		final File f = new File(path);
		d.setMessage("Really delete "+f.getName()+"?");
		d.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				PadSample ps= getPadSample4(padSampleState);
				if (ps != null) {
					ps.setSamplePath(null);
					if (padSampleState == selectedPadState) {
						setGoButtonState(ps);
					}
				}
				f.delete();
				sampleItemAdapter.removePadState(padSampleState);
			}
		});
		d.setNeutralButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show();
	}

	protected String getCurrentBankBaseDir()
	{
		String bd = "/";
		if (currentBankFile != null) {
			File currentBankDir = currentBankFile.getParentFile();
			bd = currentBankDir.getAbsolutePath();
		} else {
			File extSD = getStorageBaseDir();
			if (extSD != null) {
				bd = extSD.getAbsolutePath();
			}
		}
//		Log.d("base dir", "got bas dir as "+bd);
		return bd;
	}

	protected File getStorageBaseDir() {
		File f = new File(Environment.getExternalStorageDirectory()+"/"+"mayaswell", "grabadora");
		if (!f.exists()) {
			f.mkdirs();
			// also check noe and see if we should migrate
			File orig = getExternalFilesDir(null);
			Collection<File> existingContent = FileUtils.listFiles(orig, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
			for (File oldFile: existingContent) {
				try {
					FileUtils.moveToDirectory(oldFile, f, false);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			MediaScannerConnection.scanFile(this, new String[] {f.toString()}, null, null);
		}
		return f;
	}

	protected Action0 onCompleted = new Action0() {
		@Override
		public void call() {
//			Log.d("checkBoundingBox", "got completion");
		}
	};

	private void startPeriodicUpdate() {
		if (periodicUpdater != null && !periodicUpdater.isUnsubscribed()) {
			return;
		}
		periodicUpdater = Observable.interval(100, TimeUnit.MILLISECONDS)
				.onBackpressureDrop()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new Action1<Long>() {
			@Override
			public void call(Long aLong) {
				onPeriodicUiUpdate();
			}
		}, onError);
	}

	private void stopPeriodicUpdate() {
		if (periodicUpdater != null) {
			periodicUpdater.unsubscribe();
		}
	}


	protected void onPeriodicUiUpdate() {
//		if (sampleEditorFragment.isVisible() && wreckage != null && wreckage.isPlaying()) {
//			sampleEditorFragment.updateSampleCursor();
//		}

		if (audioMixer.isRecording() && beatCounter != null) {
			beatCounter.setText(metric.getString(audioMixer.getRecordTime()));
		}
		for (PadSample p: padSample) {
			if (p != null && p.isPlaying()) {
				SampleItemView sv = getSampleView4(p.getState());
				if (sv != null) {
					sv.updateCursor();
				}
			}
		}
	}

	/**
	 * initiate recording of a new sample
	 * @param ts
	 */
	private void startRecording(long ts) {
		recButton.setRecording(true);
		audioMixer.doRecord(
				getUniqueFile(getSaveDirectory(), sampleNameBase, lastRecFileNumber,AudioFile.Type.WAVE), ts)
			.subscribe(
				new Observer<AudioFile>() {
					@Override
					public void onCompleted() {
						Log.d("startRecording()", "onCompleted()");
						handleRecordClosed();
						recButton.setRecording(false);
					}

					@Override
					public void onError(Throwable e) {
						Log.d("startRecording()", "onError() "+ e.getMessage());
						handleErrorMessage(e.getMessage(), ErrorLevel.ERROR_EVENT);
						e.printStackTrace();
						recButton.setRecording(false);
					}

					@Override
					public void onNext(AudioFile audioFile) {
						Log.d("startRecording()", "onNext() got "+ audioFile.getPath());
						PadSample s = getFreePadSample();
						if (s != null) {
							/*)
							PadSampleState ps = s.getState();
							ps.path = audioFile.getPath();
							s.setState(ps);
							*/
							setPadSampleState(s, audioFile.getPath());

							setPadState(padSample.indexOf(s), s.state, s.getSampleInfo());
							sampleItemAdapter.onPadSampleLoaded(s);
							if (selectedPadState == null) {
								select(s.getState());
							}
						} else {
							PadSampleState ps = new PadSampleState();
							Bufferator.SampleInfo si = Bufferator.load(audioFile.getPath());
							if (si != null) {
								ps.path = si.path;
								ps.loopLength = si.nTotalFrames;
							} else {
								Log.d("Main", "unexpected null info from bufferaetor");
							}
							int i = setPadState(-1, ps, si);
							sampleItemAdapter.onPadStateAdded(ps, si);
							if (selectedPadState == null) {
								select(ps);
							}
						}
						Log.d("startRecording()", "onNext() handled ok");
					}
				});
		Log.d("Grabadora", String.format("recording started!"));

	}

	/**
	 * halt the current recording process
	 * @param ts timestamp of the initiating button press
	 */
	private void stopRecording(long ts) {
		recButton.setPrimed(true);
		audioMixer.stopRecording(ts);
		Log.d("Grabadora", String.format("recording stopped!"));
	}


	private void setPadSampleState(PadSample s, String path) {
		s.setSamplePath(path, true);
	}

	private PadSample getFreePadSample() {
		return getFreePadSample(false);
	}

	private PadSample getFreePadSample(boolean force) {
		PadSample found = null;
		for (int i=0; i<padSample.size(); i++) {
			PadSample ps = padSample.get(i);
			if (ps != null) {
				if (ps.getState().path == null || ps.getState().path.equals("")) {
					Log.d("getFreePadSample()", "return found "+ps.toString());
					return ps;
				}
				if (force) {
					if (found == null) {
						found = ps;
					} else if (found.isPlaying() && !ps.isPlaying()) {
						found = ps;
					} else if (found.isPlaying() && ps.isPlaying()) {
						// ??? TODO XXX FIXME maybe ps still a better choice but we don't have any measure of this atm
					}
				}
			}
		}
		Log.d("getFreePadSample()", "found  "+(found!=null?found.toString(): "null"));
		if (found != null) {
			SampleItemView sv = getSampleView4(found.state);
			Log.d("getFreePadSample()", "setting sample view "+((sv != null)? sv.toString(): "null"));
			if (sv != null) {
				sv.refreshDrawableState();
			}
		}
		return found;
	}

	private boolean isRecording() {
		return audioMixer.isRecording();
	}

	private File getUniqueFile(File dir, String baseName, int tryno, AudioFile.Type type)
	{
		for (;;) {
			String name = baseName;
			if (tryno > 0) {
				name += Integer.toString(tryno);
			}
			File file = new File(dir, name+"."+getFileExtension(type));
			if (!file.exists()) {
				lastRecFileNumber = tryno;
				return file;
			}
			tryno++;
		}
	}

	public void setSampleFileNameBase(String sampleFileNameBase) {
		sampleNameBase = sampleFileNameBase;
		lastRecFileNumber = 0;
	}


	/**
	 * todo fold this into a static method in the interface
	 * @param t
	 * @return
	 */
	public static String getFileExtension(AudioFile.Type t) {
		return "wav";
	}

	public File getSaveDirectory() {
		File recDir;
		if (currentBankFile == null) {
			File appDir = getStorageBaseDir();
			recDir = new File(appDir, "Recorded");
			if (!recDir.exists()) {
				recDir.mkdir();
				lastRecFileNumber = 1;
			}
		} else {
			File bankDir = currentBankFile.getParentFile();
			String bankName = currentBankFile.getName();
			if (bankName.lastIndexOf('.')>0) {
				bankName = bankName.substring(0, bankName.lastIndexOf('.'));
			}
			recDir = new File(bankDir, "Recorded_"+bankName);
			if (!recDir.exists()) {
				recDir.mkdir();
				lastRecFileNumber = 1;
			}
		}
		return recDir;
	}

	/**
	 * handles messages from buffer control, audio mixer etc.
	 * @param msg
	 * @return
	 */
	protected boolean handleErrorMessage(String msg, int severity) {

		if (currentErrorSeverity > severity) {
			Log.d("error", "skip formal display of error ");
			return true;
		}
		String title = "Bad thing happened ... oopsie";
		switch (severity) {
			case ErrorLevel.ERROR_EVENT: title = "Error"; break;
			case ErrorLevel.WARNING_EVENT: title = "Warning"; break;
			case ErrorLevel.FATAL_EVENT: title = "Minor Disaster"; break;
		}
		currentErrorSeverity = severity;
		mafFalog(msg, title);
		return false;
	}

	private void handleRecordClosed() {
		Log.d("Grabadora", String.format("recording closed"));
		recButton.setRecording(false);
	}


	/*********************************************
	 *  implementations for SamplePlayer.Manager
	 ********************************************/
	@Override
	public Context getContext() {
		return this;
	}

	@Override
	public int countSamplePlayers() {
		return 0;
	}

	@Override
	public SamplePlayer getSamplePlayer(int i) {
		return null;
	}

	@Override
	public void startSamplePlayer(SamplePlayer p, boolean b) {

	}

	@Override
	public void stopSamplePlayer(SamplePlayer p) {

	}

	@Override
	public float getSampleRate() {
		return audioMixer.getSampleRate();
	}

	/**************************
	 * ALERT DIALOG WRAPPERS
	 *************************/
	/**
	 * basic error dialog
	 * @param msg
	 * @param title
	 */
	public void mafFalog(String msg, String title)
	{
		AlertDialog.Builder d = new AlertDialog.Builder(this);
		d.setTitle(title);
		d.setMessage(msg);
		d.setPositiveButton("okidoki", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
				currentErrorSeverity = ErrorLevel.NO_ERROR;
			}
		});
		d.setCancelable(true);
		d.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				dialog.dismiss();
				currentErrorSeverity = ErrorLevel.NO_ERROR;
			}
		});
		d.show();
	}

	public void select(SampleItemView v) {
		for (int i=0; i<sampleItemView.getChildCount(); i++) {
			View v2 = sampleItemView.getChildAt(i);
			if (v2 instanceof SampleItemView) {
//				Log.d("Main()", "set select "+v.toString()+", "+v2.toString()+", selected "+(v2==v));
				v2.setSelected(v2 == v);
			}
		}
		select(v.getPadState());
	}

	public void select(PadSampleState s) {
		selectedPadState = s;
		PadSample p = getPadSample4(s);
		setGoButtonState(p);
	}

	private void setGoButtonState(PadSample p) {
		if (p != null) {
			goButton.setEnabled(true);
			if (p.isPlaying()) {
				goButton.setPlaying(true);
			} else {
				goButton.setPlaying(false);
			}
		} else {
			goButton.setEnabled(true);
			goButton.setPlaying(false);
		}
	}

	public PadSample getPadSample4(PadSampleState s) {
		if (s == null) return null;
		for (PadSample p: padSample) {
			if (p.state == s) {
				return p;
			}
		}
		return null;
	}

	protected int setPadState(int ind, PadSampleState state, Bufferator.SampleInfo info) {
		if (ind < 0) {
			ind = padStateList.size();
		}
//		if (state != null) state = state.clone();
		while (padStateList.size() <= ind) {
			padStateList.add(null);
		}
		padStateList.set(ind, state);
		while (sampleInfoList.size() <= ind) {
			sampleInfoList.add(null);
		}
		sampleInfoList.set(ind, info);
		Log.d("main", "added sample " + ind + " " + state.path);
		return ind;
	}

	private SampleItemView getSampleView4(PadSampleState state) {
		for (int i=0; i<sampleItemView.getChildCount(); i++) {
			View v2 = sampleItemView.getChildAt(i);
			if (v2 instanceof SampleItemView) {
				SampleItemView sv = (SampleItemView) v2;
				if (sv.getPadState() == state) {
					return sv;
				}
			}
		}
		return null;
	}

	private void showAbout()
	{
		PackageInfo pInfo;
		String versnm = "1.0.1";
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			versnm = pInfo.versionName;
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		String appnm = getResources().getString(R.string.app_name);
//		String appniknm = getResources().getString(R.string.release_nickname);
		showTextDialog(appnm+", "+versnm, R.string.about_text);
	}

	private void showTextDialog(String title, int contentId)
	{
		int pbi = (int) Math.floor(Math.random()*aboutButtonText.length);
		AlertDialog.Builder d = new AlertDialog.Builder(this);
		d.setTitle(title);
		d.setMessage(Html.fromHtml(getResources().getString(contentId)));
		d.setPositiveButton(aboutButtonText[pbi], new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show();
	}


}